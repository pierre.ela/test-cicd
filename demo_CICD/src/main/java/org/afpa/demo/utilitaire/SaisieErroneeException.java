package org.afpa.demo.utilitaire;

public class SaisieErroneeException extends Exception {

	  public SaisieErroneeException() {
	    super();
	  }

	  public SaisieErroneeException(String s) {
	    super(s);
	  }
	}
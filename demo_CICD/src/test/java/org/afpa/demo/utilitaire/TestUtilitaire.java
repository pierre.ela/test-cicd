package org.afpa.demo.utilitaire;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestUtilitaire {
	
	@Test
	void testAddition() {
		Double a=1D;
		Double b=2D;
		assertEquals (Utilitaire.addition(a, b), a+b, 0);
	}
	
	@Test
	void testSoustraction() {
		Double a=1D;
		Double b=2D;
		assertEquals (Utilitaire.soustraction(a, b), a-b, 0);
	}
	
	@Test
	void testMultiplication() {
		Double a=1D;
		Double b=2D;
		assertEquals (Utilitaire.multiplication(a, b), a*b, 0);
	}
	
	@Test
	void testDivision()  {
		Double a=1D;
		Double b=2D;
		try {
			assertEquals (Utilitaire.division(a, b), a/b, 0);
		}
		catch (Exception e) {
			
		}
	}

}
